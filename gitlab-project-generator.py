#!/usr/bin/python

import subprocess
import shutil
import os
import sys, getopt
import importlib
import json
import glob	

try:
    requests = importlib.import_module('requests')
except ImportError as err:
    subprocess.check_call(["pip", "install", "requests==2.21.0"])
    requests = importlib.import_module('requests')        

#Utils
def onerror(func, path, exc_info):
        """
        Error handler for ``shutil.rmtree``.

        If the error is due to an access error (read only file)
        it attempts to add write permission and then retries.

        If the error is for another reason it re-raises the error.

        Usage : ``shutil.rmtree(path, onerror=onerror)``
        """
        import stat
        if not os.access(path, os.W_OK):
            # Is the error an access error ?
            os.chmod(path, stat.S_IWUSR)
            func(path)
        else:
            raise Exception('Error')

DEFAULT_CONFIG_FILE_PATH = 'gitlab-project-generator.config.json'

INITIAL_CONFIG_TEMPLATE = {
	"private_token":"",
	"gitlab_host":"",
	"namespace":"",
	"default_project_type":"gradle-base-multiproject",
	"cretate_project_data":{
		"namespace_id":None,
		"visibility":"public",
		"issues_enabled":True,
		"merge_requests_enabled":True,
		"jobs_enabled":True,
		"container_registry_enabled":True,
		"shared_runners_enabled":True,
		"merge_method":"merge",
		"resolve_outdated_diff_discussions":False,
		"only_allow_merge_if_pipeline_succeeds":True,
		"only_allow_merge_if_all_discussions_are_resolved":True,
		"tag_list":[],
		"approvals_before_merge":1
	},
	"project_types":{
		"gradle-base-multiproject":{
			"template_git_url":"https://gitlab.com/projectgenerator/gradle-base-multiproject-template.git",
			"tag_list":["java"]
		}	
	}
}

def main(argv):
	usageHelp = 'Use as: '+argv[0]+'-p <project_type> -n <project_name> -u <namespace> -t <private_token> -d <gitlab-host>'

	config_file_name = ''

	files = glob.glob("*.config.json")

	if len(files)>0:
		if len(files)==1: 
			config_file_name=files[0]
		else:
			for idx, file in enumerate(files): print("{} {}".format(idx,file))
			config_file_name_index=input('Type index file name from the list above or press enter for create default:')
			if 	not config_file_name_index=='': config_file_name=files[int(config_file_name_index)]

	if config_file_name == '':
		with open(DEFAULT_CONFIG_FILE_PATH, 'w') as outfile:  
			json.dump(INITIAL_CONFIG_TEMPLATE,outfile, indent=4)
		print('Created config file: '+DEFAULT_CONFIG_FILE_PATH)	
		config = INITIAL_CONFIG_TEMPLATE
	else:
		with open(config_file_name) as json_file:
			config = json.load(json_file)

	project_type = ''
	project_name = ''
	private_token = config['private_token']
	gitlab_host = config['gitlab_host']
	namespace = config['namespace']

	try:
		opts, args = getopt.getopt(argv[1:],"p:n:u:t:d:",["project_type=","project_name=",'namespace=','private_token=','gitlab_host='])
	except getopt.GetoptError as err:
		print(err)
		print(usageHelp)
		sys.exit(2)
	
	for opt, arg in opts:
		if opt == '-h':
			print(usageHelp)
			sys.exit()
		elif opt in ("-p", "--project_type"):
			project_type = arg	
		elif opt in ("-n", "--project_name"):
			project_name = arg
		elif opt in ("-t", "--private_token"):
			private_token = arg	
		elif opt in ("-u", "--namespace"):
			namespace = arg
		elif opt in ("-d", "--gitlab_host"):
			gitlab_host = arg


	if project_type == ''   :	project_type = input('Project type (default {}):'.format(config['default_project_type']))
	if project_type == ''   : project_type = config['default_project_type']
	if project_name == ''   :	project_name = input('Name project:')
	if namespace == ''   :	namespace = input('Name space (user name or group name):')	
	if private_token == ''   :	private_token = input('Gitlab private token:')
	if gitlab_host == ''   :	gitlab_host = input('Gitlab host:')

	if not project_name:  raise NameError('Project name should be not empty')
	if not namespace:  raise NameError('Namespace should be not empty')
	if not private_token:  raise NameError('Private Gitlab token should be not empty')
	if not gitlab_host:  raise NameError('Gitlab host should be not empty')

	project_type_params = config["project_types"][project_type]

	template_git_url = project_type_params['template_git_url']

	
	subprocess.check_call(["git", "clone", template_git_url, project_name])

	os.chdir(os.path.join(os.getcwd(),project_name))

	shutil.rmtree('.git',onerror=onerror)

	remote_git_url = 'git@{0}:{1}/{2}.git'.format(gitlab_host,namespace,project_name)

	subprocess.check_call('git init && git add . && git commit -m "initial commit" && git remote add origin {0}'.format(remote_git_url), shell=True)

	accessParam = {'private_token':private_token}
	
	generated_project_data = {
		"name":project_name,
		"tag_list":project_type_params['tag_list']
	}

	#merge dictionaries
	project_data = {**config['cretate_project_data'], **generated_project_data}
	
	project_create_result = requests.post('https://{0}/api/v4/projects'.format(gitlab_host),params=accessParam ,data = project_data)

	project = json.loads(project_create_result.text)

	#print(project)
	
	subprocess.check_call('git push --set-upstream origin master',stdin=subprocess.PIPE, stdout=subprocess.PIPE)# Используем subprocess.PIPE для ввода пароля

	subprocess.check_call('git checkout -b develop')

	subprocess.check_call('git push --set-upstream origin develop',stdin=subprocess.PIPE, stdout=subprocess.PIPE)

	set_develop_as_default_branch_result = requests.put('https://{}/api/v4/projects/{}'.format(gitlab_host,project['id']),
		params=accessParam,
		data = {
			'default_branch':'develop'
		})

	print(set_develop_as_default_branch_result)

	protect_develop_branch_result = requests.post('https://{}/api/v4/projects/{}/protected_branches'.format(gitlab_host,project['id']),
		params=accessParam,
		data = {
			'name':'develop',
			'push_access_level':'0',
			'merge_access_level':'30'
		})

	print(protect_develop_branch_result)

	unprotect_master_branch_result = requests.delete('https://{}/api/v4/projects/{}/protected_branches/master'.format(gitlab_host,project['id']),
		params={
			'private_token':private_token
		})

	print(unprotect_master_branch_result)

	protect_master_branch_result = requests.post('https://{}/api/v4/projects/{}/protected_branches'.format(gitlab_host,project['id']),
		params=accessParam,
		data = {
			'name':'master',
			'push_access_level':'0',
			'merge_access_level':'40'
		})

	print(protect_master_branch_result)

	input('Done. Press any key')

if __name__ == "__main__":
   main(sys.argv)

